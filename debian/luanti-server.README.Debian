luanti-server for Debian
========================

Debian supports running multiple Luanti servers.

    * The default server configuration is located at /etc/luanti/default.conf
    * Worlds, mods, and other persistent game data are stored in /var/lib/luanti/
        * Due to the switch to a dynamic service user, this actually links to
          /var/lib/private/luanti and is accessible only to root. Files placed
          in the appropriate subfolder (see section below) will be changed to
          the correct owner by systemd when the service starts.
    * All log files are written to /var/log/luanti/


luanti-server systemd units
===========================

The default server service, luanti-server, should already be enabled and
running after install.

Debian's Luanti server package also includes a template unit. You can start
multiple servers with different configurations by specifying the name of your
configuration files on the command-line.

The following command will start the server with the configuration in
/etc/luanti/pvp.conf:

    systemctl start luanti-server@pvp

The new log file will then be written to /var/log/luanti/pvp/server.log.

Persistent server files will be written to /var/lib/luanti/pvp/.

To change the game loaded by the server, a separate file must be created at
/etc/luanti/pvp.env with the following line:

LUANTI_GAMEID=minetest_game

Change minetest_game to the gameid you want to load for that configuration.

NOTE: Luanti now has Prometheus metrics export support that binds to TCP port
30000 by default. If your extra servers are failing to start due to port
issues, even though you've set a different port in the configuration file, you
may need to change the prometheus_listener_address to a different port as well.


migrating from minetest-server
==============================

The Debian-minetest user is no longer used. The server service file now uses
systemd's DynamicUser.

Each template unit now uses a separate persistent game data folder under
/var/lib/luanti instead of sharing a single folder in
/var/games/minetest-server. This means server templates are more isolated and
no longer share worlds, mods, or games. Mods and games installed through Debian
packages will continue to be available to all servers.

The old default configuration (minetest-server@minetest) is migrated along with
persistent game data and logs to the new paths upon upgrade from previous
minetest versions. All other custom configurations must be migrated manually.

Upstream has removed the option to set the game from the configuration file and
it now must be passed as a command-line argument to the server. All
luanti-server templates default to minetest_game but this can be changed by
following the instructions in the previous section.

A summary of the major path changes are as follows:

- /etc/minetest/X.conf                  ->  /etc/luanti/X.conf
- /var/games/minetest-server/.minetest  ->  /var/lib/luanti/default
- /var/log/minetest/X.log               ->  /var/log/luanti/X/server.log

