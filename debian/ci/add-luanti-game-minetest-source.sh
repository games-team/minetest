#!/bin/sh

set -e

echo "deb [trusted=yes] https://salsa.debian.org/twrightsman/luanti-game-minetest/-/jobs/6899971/artifacts/raw/aptly unstable main" | tee /etc/apt/sources.list.d/luanti-game-minetest.list
apt-get update
